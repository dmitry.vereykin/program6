package com.dvereykin.program6;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;

public class DataSource extends Service {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {DatabaseHelper.COLUMN_ID,
            DatabaseHelper.COLUMN_ADDRESS, DatabaseHelper.COLUMN_NAME,
            DatabaseHelper.COLUMN_CITY, DatabaseHelper.COLUMN_STATE,
            DatabaseHelper.COLUMN_ZIPCODE, DatabaseHelper.COLUMN_IMAGE};

    DataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void initDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createAddress(PDAttributeGroup address) {
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.COLUMN_NAME, address.name);
        values.put(DatabaseHelper.COLUMN_ADDRESS, address.address);
        values.put(DatabaseHelper.COLUMN_CITY, address.city);
        values.put(DatabaseHelper.COLUMN_STATE, address.state);
        values.put(DatabaseHelper.COLUMN_ZIPCODE, address.zipCode);
        values.put(DatabaseHelper.COLUMN_IMAGE, address.image);


        long insertId = database.insert(DatabaseHelper.TABLE_ADDRESS,
                null,
                values);

        return insertId;
    }

    public void deleteAddress(PDAttributeGroup address) {
        long id = address.id;
        database.delete(DatabaseHelper.TABLE_ADDRESS,
                DatabaseHelper.COLUMN_ID + " = " + id, null);
    }

    public PersonalDataCollection getAllAddresses() throws Exception {
        PersonalDataCollection addresses = new PersonalDataCollection();

        Cursor cursor = database.query(DatabaseHelper.TABLE_ADDRESS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            addresses.addAddress(cursorToAddressAttributeGroup(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return addresses;
    }

    private PDAttributeGroup cursorToAddressAttributeGroup(Cursor cursor) {
        PDAttributeGroup address = new PDAttributeGroup(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ADDRESS)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITY)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ZIPCODE)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_IMAGE)));
        return address;
    }

    public class ServiceBinder extends Binder {
        DataSource getService() {
            return DataSource.this;
        }
    }

    private final IBinder serviceBinder = new ServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return serviceBinder;
    }

}
