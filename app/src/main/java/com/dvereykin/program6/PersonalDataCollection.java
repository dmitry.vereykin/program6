package com.dvereykin.program6;

import java.util.ArrayList;

/**
 * Created by Dmitry Vereykin aka eXrump on 10/20/2016.
 */

public class PersonalDataCollection {
    ArrayList<PDAttributeGroup> addressList = new ArrayList<PDAttributeGroup>();
    final int MAX_ADDRESS_COUNT = 15;

    public boolean isAddressLimitReached() {
        return (addressList.size() >= MAX_ADDRESS_COUNT);
    }

    public int addAddress(PDAttributeGroup address) throws Exception {

        if (isAddressLimitReached())
            throw (new Exception("Max Address Reached."));

        addressList.add(address);
        return addressList.indexOf(address);
    }

    public void setAddress(int addressIndex, PDAttributeGroup address) {
        addressList.set(addressIndex, address);
    }

    public void removeAddress(int addressIndex) {
        addressList.remove(addressIndex);
    }

    public PDAttributeGroup getAddress(int addressIndex) {
        return addressList.get(addressIndex);
    }
}

class PDAttributeGroup {
    public String name;
    public String address;
    public String city;
    public String state;
    public String zipCode;
    public String image;
    public long id;

    public PDAttributeGroup(int id, String name, String address, String city, String state, String zipCode, String image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.image = image;
    }

    public PDAttributeGroup(String name, String address, String city, String state, String zipCode, String image) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.image = image;
    }
}
